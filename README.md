# README #

After months and years of travel from home to office and back in the traffic, I did observe that there is a pattern as to when the traffic is less and therefore it takes me faster to reach to office. Unfortunately there was no fixed time in a given day when this used to happen The best bet is to use Google maps to see what was the best time to leave such as to reach with minimal commute. However, the Google maps estimated time was always as per the current traffic situation and with some statistical data pulled into it. In order to plan my travel in advance therefore I would need to build a statistical model over multiple weeks and use this to estimate the best time for travel. The primary challenge which appears is, how does one go about collecting this information from Google maps, automatically? It is not possible with a free G-API as real time data is a paid service. This Python application essentially opens up your browser for Google Maps, saves the output from the browser. Parses the text and calculates what was the estimated travel time. This script runs from a given start time to end time, and keeps logging it as a CSV format.

This CSV data can be used for modeling the estimated time of travel, given a time and day.

### What is this repository for? ###

* Quick summary
As noted this helps you avoid the GAPI calls for getting a real time data on time taken for travel.
In the previous versions it needed a GUI X-Windows to make it happen, in the latest version it is not 
required.
* Version

### How do I get set up? ###

* Summary of set up
As a must you need phantomjs 2.1.1 version to make this work.
* Configuration
* Dependencies
Phantomjs 2.1.1
Python 2.7.6
Ubuntu 14.04
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact